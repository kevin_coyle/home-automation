require('dotenv').config()
const { Client } = require('tplink-smarthome-api');
const fetch = require('node-fetch');
const schedule = require('node-schedule');
const weatherAppId = process.env.WEATHER_APP_ID;
const deviceId = process.env.DEVICE_ID;
const client = new Client();
const maxTemp = 28;


async function start() {
    const weatherInfoResponse = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=manchester&appid=${weatherAppId}&units=metric`);
    const weatherInfo = await weatherInfoResponse.json();
    const temp = weatherInfo.main.temp;
    if (temp > maxTemp) {
        return;
    }
    console.log(`Temperature is less than ${maxTemp} ${maxTemp > 1 ? 'degrees' : 'degree'}`);
    console.log('Switching On');
    client.startDiscovery().on('device-new', async (device) => {
        const info = await device.getSysInfo();
        if (info.deviceId === deviceId) {
            device.setPowerState(true);
            client.stopDiscovery();
        }
    });

}

async function stop() {
        console.log('Time to switch off');
        client.startDiscovery().on('device-new', async (device) => {
        const info = await device.getSysInfo();
        if (info.deviceId === deviceID) {
            device.setPowerState(false);
            client.stopDiscovery();
        }
    });
}

const onSchedule = '0 11 * * *';
const offSchedule = '0 12 * * *';

schedule.scheduleJob(onSchedule, start);
schedule.scheduleJob(offSchedule, stop);